<?php
    $servername="localhost";
    $username="root";
    $password="";
    $dbname="album";
    session_start();
    $con=new mysqli($servername,$username,$password,$dbname);

    if($con->connect_error)
    {
        die("Connection failed : ".$con->connect_error);
    }
    
    if(isset($_POST['logout'])){ 
        session_destroy();
        header("Location: ../album/userLogin.php");
    }
 ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            body {
  background:  url('bg.jpg');
  background-repeat: no-repeat;
  background-size: cover;}
        * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
              float: center;
              width: 25%;
              padding: 0 10px;
            }

            /* Remove extra left and right margins, due to padding in columns */
            .row {margin: 0 -5px;}

            /* Clear floats after the columns */
            .row:after {
              content: "";
              display: table;
              clear: both;
            }

            /* Style the counter cards */
            .card {
              border-radius: 1em;
              margin-top: 30px;
              box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
/*              padding: 16px;*/
              text-align: center;
              background-color: black;
              opacity: 0.8;
            }
            .card:hover{
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.8);
            }
 h3,a,h2,hr{
  color: white;
 }
  input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 20%;
  background-color: #33ADFF;
  color: white;
  padding: 10px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}
input[type=password] {
    width: 100%;
  padding: 12px 20px;
  
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}


input[type=submit]:hover {
  background-color: #008AE1;
}
a{
    text-decoration: none;
}
table, th, td {
  border: 1px solid white;
  color: white;
}
th{
    	background-color: #202020;
}
td{
    background-color: 	#505050;
}
table{
    border-collapse: collapse;  
}
        </style>
    </head>
    <body>
        <form name="myform" method="POST"> 
        <?php
        // put your code here
        
//        echo 'Welcome '.$_SESSION['usname'].'!!';
        ?>
    <center>
        <div class="row">
            <div class="column">
                <div class="card">
                    <h2>Login Successful by <?php echo $_SESSION['usname']?></h2>        
    
                </div>
            </div>
        </div> 
            <table>               
                <tbody>
                    <tr>
                        <th>Sr No</th><br>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Password</th>
            </tr><br> 
            <?php
                
                $usernamee=$_SESSION['usname'];
     
                $query= "select * from user_info where user_name= '$usernamee'";
                 
                $result = mysqli_query($con, $query);
                $row=(mysqli_fetch_array($result));
            ?>
                    <tr>
                        <td><?php echo $row['sr_no'];?></td>
                        <td><?php echo $row['name'];?></td>
                        <td><?php echo $row['email'];?></td>
                        <td><?php echo $row['user_name'];?></td>
                        <td><?php echo $row['password'];?></td>
                        </td>                    
                    </tr>
                    
                </tbody>
            </table>
                <div class="row">
                    <div class="column">
                        <div class="card">
                            <a href="album.php" style="font-size: 20px;font-weight: bold;">Album</a> <br>
                            <a href="viewallimages.php" style="font-size: 20px;font-weight: bold;">View All Images</a><br>
                            <a href="viewbybirthday.php" style="font-size: 20px;font-weight: bold;">View By Birthday</a><br>
                            <a href="viewbychristmas.php" style="font-size: 20px;font-weight: bold;">View By Christmas</a><br>
                            <a href="viewbynewyear.php" style="font-size: 20px;font-weight: bold;">View By New Year</a><br>
                            <a href="viewbydiwali.php" style="font-size: 20px;font-weight: bold;">View By Diwali</a><br>
                        </div>
                    </div>
                </div>
                
            <input type='submit' id='logout' name='logout' value='logout'> 
               
        
            
    </center>
    </form>
    </body>
</html>
