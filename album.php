<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            body {
                background:  url('bg.jpg');
                background-repeat: no-repeat;
                background-size: cover;
            }
            body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: center;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding in columns */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Style the counter cards */
.card {
  border-radius: 1em;
  margin-top: 30px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
  padding: 16px;
  text-align: center;
  background-color: black;
  opacity: 0.8;
}
.card:hover{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.8);
}
 h3,a,h2,hr{
  color: white;
 }
  input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 20%;
  background-color: #33ADFF;
  color: white;
  padding: 10px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}
input[type=password] {
    width: 100%;
  padding: 12px 20px;
  
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}


input[type=submit]:hover {
  background-color: #008AE1;
}
a{
    text-decoration: none;
}
        </style>
    </head>
    <body>
    <center>
        <form>
                <div class="row">
                    <div class="column">
                        <div class="card">
                            <h2>Select Category to upload image</h2>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="column">
                        <div class="card">
                            <a href="uploadbybirthday.php" style="font-size: 20px;font-weight: bold;">Upload By Birthday</a><br>
                            <a href="uploadbychristmas.php" style="font-size: 20px;font-weight: bold;">Upload By Christmas</a><br>
                            <a href="uploadbynewyear.php" style="font-size: 20px;font-weight: bold;">Upload By New Year</a><br>
                            <a href="uploadbydiwali.php" style="font-size: 20px;font-weight: bold;">Upload By Diwali</a><br>
                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="column">
                        <div class="card">
                            <a href="loginSucess.php" style="font-size: 20px;font-weight: bold;">Back</a><br>
                        </div>
                    </div>
                </div>
        </form>
    </center>
    </body>
</html>
