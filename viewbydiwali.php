
<?php
    $servername="localhost";
    $username="root";
    $password="";
    $dbname="album";
    error_reporting(0);
    $con=new mysqli($servername,$username,$password,$dbname);

    if($con->connect_error)
    {
        die("Connection failed : ".$con->connect_error);
    }
   
 ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title><link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
        <style>
            body {
                background:  url('bg.jpg');
                background-repeat: no-repeat;
                background-size: cover;
            }
            th{
                color: white;
                font-size: 30px;
                font-family: 'Montserrat', sans-serif;
            }
             a{
                color: white;
                text-decoration: none;
            }
            .column {
              float: center;
              width: 10%;
              padding: 0 10px;
            }

            /* Remove extra left and right margins, due to padding in columns */
            .row {margin: 0 -5px;}

            /* Clear floats after the columns */
            .row:after {
              content: "";
              display: table;
              clear: both;
            }

            /* Style the counter cards */
            .card {
              border-radius: 1em;
              margin-top: 30px;
              box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
/*              padding: 16px;*/
              text-align: center;
              background-color: black;
              opacity: 0.8;
            }
            .card:hover{
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.8);
            }
            hr{
                color: white;
            }
        </style>
    </head>
    <body>
    <center>   
        <div class="row">
                    <div class="column">
                        <div class="card">
            <table >
               
                <tbody>
                    <tr>
                        <th>Diwali Images</th> 
                
            </tr><br> 
                
            <?php
             session_start();
                $usernamee=$_SESSION['usname'];                
                $query= "select image_name from album where user_name= '$usernamee' and album_type='Diwali'";    
                $result = mysqli_query($con, $query);
                while ($row = mysqli_fetch_array($result)) {
            ?>
                
                    <tr>                        
                        <td><a href="image/<?php echo $row['image_name'];?>" target="_blank"><img src="image/<?php echo $row['image_name'];?>" height="100px" width="100px" style="border-radius: 30px"></a>
                        </td>                    
                    </tr>
                    
                <?php                
                }
                ?>
    
                </tbody>
                
            </table>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="card">
                    <a href="loginSucess.php" style="font-size: 30px;font-weight: bold;">Back</a>                    
                </div>
            </div>
        </div>
        </center>
    </body>
</html>
