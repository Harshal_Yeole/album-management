<?php
    $servername="localhost";
    $username="root";
    $password="";
    $dbname="album";
//    error_reporting(0);
    $con=new mysqli($servername,$username,$password,$dbname);

    if($con->connect_error)
    {
        die("Connection failed : ".$con->connect_error);
    }
    
     if(isset($_POST['submit'])){
         session_start();
         $usernamee=$_SESSION['usname'];
             $fileExtensions = ['JPEG', 'JPG', 'PNG', 'jpeg', 'jpg', 'png']; // Get all the file extensions

    if (!empty($_FILES['myfile']['name'])) {

        $name = $_FILES['myfile']['name'];
        $fileSize = $_FILES['myfile']['size'];
        $fileTmpName = $_FILES['myfile']['tmp_name'];
        $fileType = $_FILES['myfile']['type'];
        $fileExtension = strtolower(end(explode('.', $name)));


        if (!in_array($fileExtension, $fileExtensions)) {
            ?>
                    <script>
                        alert("Invalid file extention");
                    </script>

            <?php
        } else {

            //......... upload file........
            $gettime = date("Y-m-d-H.i.s");
            $finalname=$usernamee."_birthday_".$gettime;
            $name = str_replace("$name", "$finalname.png", "$name");
            $uploadPath = "./image/" . $name;
            $fileName = $name;
            echo $fileName;
            move_uploaded_file($fileTmpName, $uploadPath);
               $query ="INSERT INTO `album`(`user_name`, `album_type`, `image_name`) VALUES ('$usernamee','Birthday','$fileName')";
            if($con->query($query)){
                echo 'Data saved';
            }
            else{
                echo 'data not saved '.$con->error;
            }
        }
    }
     }
   
 ?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
        <style>
            body {
                background:  url('bg.jpg');
                background-repeat: no-repeat;
                background-size: cover;
            }
            label {
                 font-family: 'Montserrat', sans-serif;
                background-color: #ff5959;
                color: white;
                padding: 0.5rem;
                font-family: sans-serif;
                border-radius: 1em;
                cursor: pointer;
                margin-top: 1rem;
              }
            label:hover {
                background-color: #ff6e6e;
            }
            input[type="submit"] {
                border: 1px solid #0A3D5B;
                color: #0A3D5B;
                font-size: 20px;
                font-weight: 300;
                background: white;
                cursor: pointer;
                display: inline-block;
                border-radius: 1em;
                text-transform: capitalize;}
            h1,a{
                  font-family: 'Montserrat', sans-serif;
              }
            a{
                color: white;
    text-decoration: none;
}
.column {
  float: center;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding in columns */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Style the counter cards */
.card {
  border-radius: 1em;
  margin-top: 30px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
  padding: 16px;
  text-align: center;
  background-color: black;
  opacity: 0.8;
}
.card:hover{
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.8);
}
        </style>
    </head>
    <body>
    <center>
        <form method="POST" enctype="multipart/form-data">
            <h1>Upload Birthday Photos</h1>
            <label><input type="file" id="myfile" name="myfile" hidden >Choose File</label>
            <input type="submit" name="submit" id="submit" value="submit"><br>
        </form>
        <div class="row">
            <div class="column">
                <div class="card">
                    <a href="album.php" style="font-size: 30px;font-weight: bold;">Back</a>                    
                </div>
            </div>
        </div>
    </center>
    </body>
</html>
